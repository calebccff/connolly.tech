from os import listdir
from os.path import isfile, join

added = """---
title: "{0}"
date: 2019-04-20T16:53:41+01:00
draft: false
---

"""

for mypath in ["content/maths", "content/physics"]:
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for f in [x for x in onlyfiles if ".md" in x ]:
        print("Converting:", f)
        title = f[:f.index(".md")].replace("-", "")
        print("BEFORE: "+title)
        title = ''.join(map(lambda x: x if x.islower() or any(p in x for p in [".", " ", " "]) or x.isdigit() else " "+x, title))
        
        content = added.format(title)+open(mypath+"/"+f, "r").read()
        #print("\n\nWould add: \n"+content+"\nto the file")
        fi = open(mypath+"/"+f, "w")
        fi.write(content)
        fi.close()