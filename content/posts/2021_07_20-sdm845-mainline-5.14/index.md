---
title: "Linux 5.14 - The state of mainline on SDM845 devices"
date: 2021-07-20T19:55:40+01:00
draft: false
resources:
- name: header
  src: images/header.jpg
  title: "The OnePlus 6 out in the wild running Phosh (@calebccff)"
---

Summarising the state of the Linux kernel on SDM845 mobile devices as of Linux [`5.14-rc2`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/?h=v5.14-rc2).

<!--more-->

## Overview

This is the first of what will hopefully be fairly regular posts about the state of mainline on SDM845. Over the last few months we've seen [audio support appear for the OnePlus 6](https://twitter.com/calebccff/status/1396141601576083460), the [now almost upstream haptics driver](https://lore.kernel.org/linux-arm-msm/20210618175041.323495-1-caleb@connolly.tech/T/) as well as finally [finding UART on the OP6](https://twitter.com/calebccff/status/1394015022192504834).

[Joel](https://twitter.com/joelselvaraj95) has been hard at work helping the wonderful [Aleksander Morado](https://twitter.com/aleksander0m) finish up support for ModemManager with our modems.

We also have enabled support for three new SDM845 devices in our kernel!
The [Xiaomi Mi 8 Pro (Equuleus)](https://gitlab.com/sdm845-mainline/linux/-/commit/21e0cd5baac468132250a4231a15094e68c91b61) thanks to [@AlexanderGP aka DeltaSporos](https://gitlab.com/AlexanderGP).
And the [LG G7 and LG V35](https://gitlab.com/sdm845-mainline/linux/-/commit/c709f13465f55b88a7df2d64e9e4afc176755194) thanks to [@Jenneron](https://gitlab.com/jenneron).

On the [postmarketOS](https://postmarketOS.org) side of things, we've enabled support for [automatic flashing of kernel updates](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2215) (including support for A/B devices), as well as gained "community" support status for the OnePlus 6 and PocoPhone F1! Culminating in pmOS becoming an ever more usable OS on these devices.

All in all we're well on our way to a very usable and solid experience on these devices, with a properly sustainable software experience - where the ownership lies just a bit closer to the users.

Unfortunately there are still some issues to work through in the kernel, [the new fw_devlink feature](https://linuxplumbersconf.org/event/7/contributions/787/), which is now enabled by default since 5.13 is used to more inteligently handle hardware dependencies (clocks, regulators etc) and manipulate probe orders among other things, actually [breaks the DSI connector clock initialisation on MSM DRM devices](https://lore.kernel.org/linux-arm-msm/CAF6AEGvnL9mOp3vKuSvz=KDijtFVMdKCF4BXSLxXeoR38O-SUg@mail.gmail.com/) causing the display to never come up on at least SDM845, MSM8998 and MSM8996 devices and requiring we pass "`fw_devlink=permissive`" on the kernel cmdline to have a working display on 5.14.

## Other work

Outside of our kernel, [Vinod Koul](https://twitter.com/vkoulk) has been busy [enabling support for Display Stream Compression](https://lore.kernel.org/linux-arm-msm/20210715065203.709914-1-vkoul@kernel.org/), a lossless compression used to drive high resolution panels, it's used on the Pixel 3 (blueline) which he works on with AOSP.

The Pixel 3 (blueline) is also shaping up to be one of the [first devices supported by nixOS mobile](https://github.com/NixOS/mobile-nixos/pull/361)! A pretty exciting option for Linux on a phone.

## Patches

As of Linux 5.14-rc2 we carry ~43 patches on top of torvalds/master, of those we are responsible for ~25 of them. Here's an overview of the patches and what they're for:

```
Amit Pundir (1):
      FROMLIST: ath10k: qmi: Skip host capability request for Xiaomi Poco F1
```
This quirk fixes a bug causing a crash when requesting the capabilities of the wifi hardware. It should hopefully land upstream soon!

```
AngeloGioacchino Del Regno (3):
      dt-bindings: Add vendor prefix for Novatek Microelectronics Corp.
      Input: Add Novatek NT36xxx touchscreen driver
      dt-bindings: touchscreen: Add binding for Novatek NT36xxx series driver
```
These patches are a port of one of the touch screen drivers needed on the PocoPhone F1. They will land upstream once someone has time to clean them up and address comments.

```
Caleb Connolly (12):
      dt-bindings: input: add Qualcomm SPMI haptics driver
      input: add Qualcomm SPMI haptics driver
      arm64: dts: qcom: pmi8998: introduce spmi haptics
      arm64: dts: qcom: sdm845-oneplus-common: add haptics
```
This patches are the latest version of the series to bring up the haptics hardware. They should land upstream soon!
```
      power: supply: add support for Qcom SPMI Fuel Guage driver
      arm64: dts: qcom: sdm845-oneplus: enable pmi8998_fg
```
These patches a part of the series to enable the battery "fuel gauge" driver which reads the battery state as well as detecting when a charger is attached. Cleaning up this series and upstreaming it is on the TODO list.
```
      arm64: configs: add sdm845 config fragment
```
This patch will never be upstreamed, it contains our kernel config fragment which produces a minimal .config for use with mobile distros, disabling other architectures and enabling the hardware required for our devices. Part of my efforts in the future will be to enable most of our drivers in the arm64 defconfig.
```
      arm64: dts: qcom: sdm845-oneplus: add ipa firmware names
      arm64: dts: qcom: sdm845-oneplus: fix reserved-mem
      dts: arm64: sdm845-oneplus-common: enable debug UART
      drm/panel/samsung-sofef00: make gpio optional
      arm64: dts: qcom: sdm845-oneplus-fajita: remove panel reset gpio
```
These patches will hopefully [appear upstream soon](https://lore.kernel.org/linux-arm-msm/20210720153125.43389-1-caleb@connolly.tech/T/) too! They are mostly minimal fixups to the OnePlus 6 devicetree, as well as enabling a workaround for the OnePlus 6T display panel which refuses to turn on after being reset.

```
Dr. Git (2):
      arm64: dts: qcom: sdm845-oneplus-common: enable audio
      ASoC: qcom: sdm845: set quaternary MI2S codec DAI to I2S format
```
These two patches apply a fix needed for the speaker driver to work on the OnePlus 6, as well as hooking up audio in the devicetree source. Many thanks to DrGit for his fantastic and relentless research to discover and apply these fixes! He has been a quiet but persistent member of the team and has done a lot of great work.

```
Joel Selvaraj (7):
      arm64: dts: qcom: sdm845-xiaomi-beryllium: add haptics
      arm64: dts: pmi8998: introduce Qcom SPMI FG driver
      input: touchscreen: nt36xxx: temporarily disable PM functions
      input: nt36xxx: fix pressure not being reported for 1 or 2 finger taps
      arm64: dts: qcom: pmi8998: Add nodes for QCOM WLED backlight
      ASoC: codecs: tas2559: Add initial tas2559 audio amplifier codec
      arm64: dts: qcom: sdm845-xiaomi-beryllium: dts changes
```
These patches from Joel mostly consist of to-be-upstreamed fixes for the Poco F1, improving the touch screen, enabling the display backlight and audio support.
```
Sporos (1):
      added initial support for Xiaomi Mi 8 Pro (Equuleus)
```
See above!

```
Thara Gopinath (6):
      firmware: qcom_scm: Introduce SCM calls to access LMh
      thermal: qcom: Add support for LMh driver
      cpufreq: qcom-cpufreq-hw: Add dcvs interrupt support
      arm64: boot: dts: qcom: sdm45: Add support for LMh node
      arm64: boot: dts: qcom: sdm845: Remove cpufreq cooling devices for CPU thermal zones
      dt-bindings: thermal: Add dt binding for QCOM LMh
```
This is an exciting series, it will be landing upstream soon hopefully, it enabled support for programming the device firmware to relax the by default extremely strict thermal and power limits impose on the CPU cores, [doing this leads to a 30-70% performance improvement](https://lore.kernel.org/linux-arm-msm/20210708120656.663851-1-thara.gopinath@linaro.org/)! A big win for us.

```
Vinod Koul (7):
      soc: qcom: geni: move GENI_IF_DISABLE_RO to common header
      soc: qcom: geni: move struct geni_wrapper to header
      soc: qcom: geni: Add support for gpi dma
      spi: spi-geni-qcom: Add support for GPI dma
      i2c: qcom-geni: Add support for GPI DMA
      arm64: dts: qcom: sdm845: Add gpi dma node
      arm64: dts: qcom: sdm845: enable dma for spi
```
This series from Vinod enables the "GPI DMA" mode on the Geni hardware block, allowing for faster larger data transfers over i2c and spi. This series has been floating about the mailing lists since Linux 5.8.. I personally will be extremely excited [when it lands upstream](https://lore.kernel.org/linux-arm-msm/20210625052213.32260-1-vkoul@kernel.org/)!

```
jenneron (1):
      arm64: dts: qcom: add support for LG G7 and LG V35
```
See above

```
venji10 (3):
      gpu: drm: panel: Add EBBG FT8719 panel driver
      input: touchscreen: Add Focaltech FT8719 touchscreen driver
      arm64: dts: beryllium: split devicetree for Tianma and EBBG variants
```
These patches enable support for the less common panel and touchscreen found in the PocoPhone F1, ported from downstream sources.

## In conclusion

It's been quite a busy few months, with so much to do, but I'm hopeful things will start to calm down as we settle and start to work on cleaning up and upstreaming the patches we do have.

I hope you enjoyed this rough overview of the state of mainline on some of our SDM845 devices, I'd love to hear your feedback and suggestions on this format, things you'd like to see included etc.

~Caleb