---
title: "{{ replace .Name "-" ". " | title }}"
date: {{ .Date }}
type: "notes"
layout: "single"
author: "Caleb Connolly"
draft: false
---
