---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
type: "project"
layout: "single"
draft: false
---
